ENVIRONMENT
=================
1.The application was tested in "net04.utdallas.edu" server.



CONFIGURATIONS
=================
1.The topology file should be placed in the project root i.e "advancecomputernetworksproject" folder with name "topology.txt" 
2.The project_run.sh file will start all the "Nodes" and "Controller".
3.The topology file path should be given to Controller class which is the last line, if using default above dont change.
	java -XX:ParallelGCThreads=1 -XX:ConcGCThreads=1 -Xmx5m -cp out/ edu.chetan.Controller ~/advancecomputernetworksproject/topology.txt
4.For configuring Nodes please change the arguments i.e after "edu.chetan.Node", instead of entire line
	java -XX:ParallelGCThreads=1 -XX:ConcGCThreads=1 -Xmx5m -cp out/ edu.chetan.Node 0 4 'A message from 0 to 4' 40 &
  


COMPILING
=================
1.unzip the file into any directory
2.Go into the extracted folder "advancecomputernetworksproject". Then issue the commands in the following order in the extracted folder root.
	mkdir out/
	javac -d out/ src/edu/chetan/Controller.java
	javac -d out/ -cp out/ src/edu/chetan/Node.java
3.The above commands will have created ".class" files in "advancecomputernetworksproject/out/edu/chetan".



RUNNING
================
1.The "topology.txt" file present is for sample scenario which was provided in the project description,
  and "project_run.sh" is used for running and it has the nodes description.
2.To run the application, use the following command from project root folder i.e. "advancecomputernetworksproject".
	bash project_run.sh


OUTPUT
================
1.The output will be placed in the "results" folder in the users home directory.
2.The output folder will be automatically deleted when "project_run.sh" is executed.



NOTE
================
1.By default topology.txt and project_run.sh files placed in the root are for sample scenario.
2.The apllication when started properly will display "timer" which starts from "6",
  this can be used to see the time since the application started and see if the program is hung.
	Timer: 6
	Timer: 7
	Timer: 8
	Timer: 9

3.Application will terminate after 120 second automatically.


---> SOMETIMES TIMER WILL NOT BE UPDATED ON THE DISPLAY THIS HAPPENS IF SSH CONNECTION HAS ISSUES AND ITS NOT THE PROBLEM OF THE PROGRAM.
     IF THIS HAPPENS KINDLY WAIT FOR A MINUTE IT MIGHT RECONNECT, ELSE PLEASE CLOSE SSH,RECONNECT AND RE-RUN.

---> FOR CONVENIENCE I HAVE PREPARED topology.txt AND project_run.sh FILES FOR "SAMPLE SCENARIO", "SCENARIO1" AND "SCENARIO2"
     WHICH WAS PROVIDED TO US IN THE PROJECT DESCRIPTION. THESE ARE PLACED IN PREPARED_FILES FOLDER IN PROJECT ROOT.

---> IF GIT IS CONVENIENT THEN USE "git clone https://gitlab.com/ChetanGarikapati/advancecomputernetworksproject.git"
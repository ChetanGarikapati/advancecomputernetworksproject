package edu.chetan;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

import static edu.chetan.Controller.DEFAULT_FILE_EXTENSION;
import static edu.chetan.Controller.OUTPUT_DIRECTORY;

/**
 * This is a test class to help with debugging of Node.
 *
 * @author Chetan Garikapati.
 */
public class Test {
    public static void main(String[] args) throws IOException, InterruptedException {

        //added to test run from terminal
        Thread.sleep(2000);

        File toMyNodeFile = new File(OUTPUT_DIRECTORY + "to" + 3 + DEFAULT_FILE_EXTENSION);
        BufferedWriter toMyNodeFileWriter = new BufferedWriter(new FileWriter(toMyNodeFile));

        Logger logger = Logger.getLogger("Test");

        String[] messages;
        messages = new String[]{
                "* 2 HELLO UNIDIR <neighbour>  <neighbour> BIDIR <neighbour> 1 <neighbour> MPR <neighbour> 1 <neighbour>",
                "* 2 HELLO UNIDIR <neighbour> 3 <neighbour> BIDIR <neighbour> 1 <neighbour> MPR <neighbour> 1 <neighbour>",
                "* 2 TC 2 1 MS <msnode> 1 <msnode>",
                "* 2 HELLO UNIDIR <neighbour>  <neighbour> BIDIR <neighbour> 1,3 <neighbour> MPR <neighbour> 1,3 <neighbour>",
                "* 2 TC 4 1 MS <msnode> 5 <msnode>",
                "2 9 DATA 9 4 message from 9",
                "* 6 HELLO UNIDIR <neighbour>  <neighbour> BIDIR <neighbour> 7 <neighbour> MPR <neighbour> 4 <neighbour>",
                "* 6 HELLO UNIDIR <neighbour> 3 <neighbour> BIDIR <neighbour> 7 <neighbour> MPR <neighbour> 4 <neighbour>",
                "* 6 TC 2 1 MS <msnode> 1 <msnode>",
                "* 6 HELLO UNIDIR <neighbour>  <neighbour> BIDIR <neighbour> 7,3 <neighbour> MPR <neighbour> 4,3 <neighbour>",
        };

        for (int i = 0; i < messages.length; i++) {
            toMyNodeFileWriter.write(messages[i]);
            toMyNodeFileWriter.newLine();
            toMyNodeFileWriter.flush();
            logger.info("Message: " + messages[i]);
            Thread.sleep(1000);
        }
    }
}

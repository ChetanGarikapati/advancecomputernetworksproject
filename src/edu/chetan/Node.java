package edu.chetan;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static edu.chetan.Controller.*;

/**
 * This class acts as a node in the OLSR network and can perform routing and topology updates independently.
 * Spawn multiple Nodes to simulate the network.
 *
 * @author Chetan Garikapati
 */
public class Node {

    //STANDARD NODE PROPERTIES
    private int nodeId;
    private int destinationNodeId;
    private int timeToSend;
    private String data;
    private int sequenceNumber;

    //FILE READERS AND WRITER
    private BufferedReader toMyNodeFileReader;
    private BufferedWriter fromMyNodeFileWriter;
    private BufferedWriter myNodeReceivedFileWriter;

    //HELPER PROPERTIES FOR OLSR
    private final HashMap<Integer, Set<Integer>> uniDirectionalNeighbours = new HashMap<>();
    private final HashMap<Integer, Set<Integer>> biDirectionalNeighbours = new HashMap<>();
    private final HashMap<Integer, List<Integer>> topologyTable = new HashMap<>(); /* Index sequence : holdtime,Seq Num,from Node,MPRS List*/
    private final TreeMap<Integer, Integer> biDirectionalNeighbourTwoHopCount = new TreeMap<>();
    private final HashSet<Integer> twoHopNeighbours = new HashSet<>();
    private final HashSet<Integer> msSet = new HashSet<>();
    private final HashSet<Integer> mprSet = new HashSet<>();
    private final Deque<String> messagesToSend = new ArrayDeque<>();

    private final ConcurrentHashMap<Integer, Integer> routingTable = new ConcurrentHashMap<>(); /* Index sequence : next hop, distance */
    private final ConcurrentHashMap<Integer, Integer> helloTimer = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Integer, Integer> tcTimer = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Integer, List<String>> queuedMessages = new ConcurrentHashMap<>();

    private static final Logger LOGGER = Logger.getLogger("Node");

    /**
     * This will start the node and controls the Node operations
     *
     * @param args The program requires atleast 2 arguments and max 4. <br>
     *             1. Current NodeId <br>
     *             2. Destionation NodeId to which message should be sent. Can be same as 1. <br>
     *             3. The message data that should be sent.This is Optional entry. <br>
     *             4. The time at which the node should attempt to send the message to destination.
     *             Optional but cannot be empty if 3 is present.
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            LOGGER.info(() -> "Insufficient Arguments passed,requires atleast 2");
            System.exit(0);
        }

        Node node = new Node();

        node.nodeId = Integer.parseInt(args[0]);
        node.destinationNodeId = Integer.parseInt(args[1]);
        if (args.length >= 3) {
            node.data = args[2];
            node.timeToSend = Integer.parseInt(args[3]);
        }

        int timer = 0; //time to run for
        try {
            node.createFiles();

            while (timer < 120) {
                node.incrementTimer();
                node.readMessages();
                node.removeTimerExpiredEntries();
                node.prepareMPRSet();
                node.prepareRoutingTable();
                node.sendHelloMessage();
                node.sendTCMessage();
                if (node.timeToSend == timer && node.data != null) node.sendDataMessage();
                if (timer % 30 == 0) node.checkQueuedMessages();
                node.transmitMessages();
                timer++;
                Thread.sleep(1000);

                LOGGER.finest(() -> new StringBuilder().append("Debug Info : ").append(node).toString());
            }

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Unexpected Error for Node: " + node.nodeId, e);
        }

    }

    /**
     * Updates timers for all entries to keep track of outdated records
     */
    private void incrementTimer() {
        helloTimer.keySet().forEach(neighbor -> helloTimer.put(neighbor, helloTimer.get(neighbor) + 1));
        tcTimer.keySet().forEach(neighbor -> tcTimer.put(neighbor, tcTimer.get(neighbor) + 1));

        LOGGER.log(Level.FINEST, "HELLO TIMERS : ", helloTimer);
        LOGGER.log(Level.FINEST, "TC TIMERS : ", tcTimer);
    }

    /**
     * Reads messages from fromX.txt file and passes them accordingly to be processed.
     */
    private void readMessages() {
        String[] processedLine;

        try {
            while (toMyNodeFileReader.ready()) {
                String temp = toMyNodeFileReader.readLine();
                processedLine = temp.split(SPACE);
                switch (processedLine[2]) {
                    case MESSAGE_TYPE_HELLO:
                        processHelloMessage(processedLine);
                        break;
                    case MESSAGE_TYPE_TC:
                        processTopologyMessage(processedLine);
                        break;
                    case MESSAGE_TYPE_DATA:
                        processDataMessages(processedLine);
                        break;
                }
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error reading messages in node : " + nodeId, e);
        }
    }

    /**
     * Removes all expired entries from routing and topology tables, if messages are not received for specified time.
     */
    private void removeTimerExpiredEntries() {
        helloTimer.forEach((neighbor, timer) -> {
            if (timer >= 15) {
                uniDirectionalNeighbours.remove(neighbor);
                biDirectionalNeighbours.remove(neighbor);
                routingTable.remove(neighbor);
                mprSet.remove(neighbor);
                msSet.remove(neighbor);

                routingTable.forEach((destination, nextHop) -> {
                    if (nextHop == neighbor) routingTable.remove(destination);
                });

                helloTimer.remove(neighbor);
            }
        });

        tcTimer.forEach((node, timer) -> {
            if (timer >= 45) {
                uniDirectionalNeighbours.remove(node);
                biDirectionalNeighbours.remove(node);
                routingTable.remove(node);
                msSet.remove(node);
                mprSet.remove(node);

                routingTable.forEach((destination, nextHop) -> {
                    if (nextHop == node) routingTable.remove(destination);
                });
                tcTimer.remove(node);
            }
        });
    }

    /**
     * Process data messages and identifies if they are for current node or if they should be routed.
     *
     * @param processedLine This string arrays is the received message,split on "SPACE" to be processed easily.
     */
    private void processDataMessages(String[] processedLine) {
        int destinationNode = Integer.parseInt(processedLine[4]);
        if (destinationNode == nodeId) {
            try {
                //THIS CAN BREAK MESSAGE IF IT HAS SPACES, SO JOINING THE ENTIRE MESSAGE BACK BEFORE WRITING OUT
                myNodeReceivedFileWriter.write(Stream.of(processedLine).skip(5)
                        .reduce((partOne, partTwo) -> partOne + SPACE + partTwo).orElse(SPACE));
                myNodeReceivedFileWriter.newLine();
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Writing to received file failed", e);
            }
        } else if (routingTable.containsKey(destinationNode)) {
            processedLine[1] = String.valueOf(nodeId); // change from node value
            processedLine[0] = String.valueOf(routingTable.get(destinationNode)); //replace nexthop with entry from routing table
            Stream.of(processedLine).reduce((partOne, partTwo) -> partOne + SPACE + partTwo)
                    .ifPresent(messagesToSend::add);
        } else {
            processedLine[1] = String.valueOf(nodeId); // change from node value
            //ADD THE MESSAGE TO WAITING QUEUE TO TRY AFTER 30 SEC
            Stream.of(processedLine).reduce((partOne, partTwo) -> partOne + SPACE + partTwo)
                    .ifPresent(dataMessage -> {
                        if (!queuedMessages.containsKey(destinationNode)) {
                            ArrayList<String> message = new ArrayList<>();
                            message.add(dataMessage);
                            queuedMessages.put(destinationNode, message);
                        } else {
                            queuedMessages.get(destinationNode).add(dataMessage);
                        }
                    });
        }
    }

    /**
     * Updates and creates routing table.
     */
    private void prepareRoutingTable() {
        //ADD 1 HOP NEIGHBORS TO ROUTING INFO
        biDirectionalNeighbours.keySet().forEach(neighbor -> {
            routingTable.put(neighbor, neighbor); // as its one hop away next hop is neighbour itself
        });

        uniDirectionalNeighbours.keySet().forEach(neighbor -> {
            if (!routingTable.containsKey(neighbor)) {
                routingTable.put(neighbor, neighbor); // as its one hop away next hop is neighbour itself
            }
        });

        topologyTable.forEach((sourceNode, tcInfo) -> {
            //ADD ROUTING ENTRY IF ONLY FROM NODE IS IN BIDIRECTIONAL NEIGHBOURS OR ELSE WE CANT SEND ALONG THE NODE
            if (!routingTable.containsKey(sourceNode) && biDirectionalNeighbours.containsKey(tcInfo.get(1))) {
                routingTable.put(sourceNode, tcInfo.get(1)); //gives the node from which we received tc message
                for (int i = 2; i < tcInfo.size(); i++) {
                    if (!routingTable.containsKey(tcInfo.get(i))) {
                        routingTable.put(tcInfo.get(i), tcInfo.get(1)); //index 1 is "from node", each 'i' is its MS set entry
                    }
                }
            }
        });

        //clear after processing
        topologyTable.clear();
        LOGGER.log(Level.FINEST, "Routing table info for node " + nodeId + " " + routingTable);

    }

    /**
     * This method select MPR's from bidirectional neighbours.
     */
    private void prepareMPRSet() {
        routingTable.clear();
        biDirectionalNeighbourTwoHopCount.clear();

        Set<Integer> biDirectionNeighborsList = biDirectionalNeighbours.keySet();
        Set<Integer> uniDirectionalNeighboursList = uniDirectionalNeighbours.keySet();

        //REMOVE ONE HOP NEIGHBOURS IF THEY ARE ADDED IN TWO-HOP LINKS
        uniDirectionalNeighbours.keySet().forEach(neighbor -> {
            twoHopNeighbours.remove(neighbor);
            uniDirectionalNeighbours.get(neighbor).removeAll(biDirectionNeighborsList);
            uniDirectionalNeighbours.get(neighbor).removeAll(uniDirectionalNeighboursList);
        });
        biDirectionalNeighbours.keySet().forEach(neighbor -> {
            twoHopNeighbours.remove(neighbor);
            biDirectionalNeighbours.get(neighbor).removeAll(biDirectionNeighborsList);
            biDirectionalNeighbours.get(neighbor).removeAll(uniDirectionalNeighboursList);

            //STORE THE NEIGHBOUR WHICH CAN REACH MAX 2 HOP NEIGHBOURS
            int maxTwoHopReachableNodeCount = uniDirectionalNeighbours
                    .containsKey(neighbor) ? uniDirectionalNeighbours.get(neighbor).size() + biDirectionalNeighbours.get(neighbor).size() :
                    biDirectionalNeighbours.get(neighbor).size();

            biDirectionalNeighbourTwoHopCount.put(maxTwoHopReachableNodeCount, neighbor);
        });


        while (twoHopNeighbours.size() > 0 && !biDirectionalNeighbourTwoHopCount.isEmpty()) {
            Integer mpr = biDirectionalNeighbourTwoHopCount.get(biDirectionalNeighbourTwoHopCount.firstKey());
            if (!mprSet.isEmpty() && !mprSet.contains(mpr)) {
                biDirectionalNeighbours.get(mpr).removeAll(routingTable.keySet());
                uniDirectionalNeighbours.get(mpr).removeAll(routingTable.keySet());
                if (biDirectionalNeighbours.get(mpr).isEmpty() && uniDirectionalNeighbours.get(mpr).isEmpty()) {
                    biDirectionalNeighbourTwoHopCount.remove(mpr);
                    continue;
                }
            }
            mprSet.add(mpr);

            //ADDING INFO TO ROUTE TABLE
            biDirectionalNeighbours.get(mpr).forEach(neighbour -> {
                if (!routingTable.containsKey(neighbour)) {
                    routingTable.put(neighbour, mpr); //mpr is next hop for all neighbors of mpr
                }
            });
            twoHopNeighbours.removeAll(biDirectionalNeighbours.get(mpr));
            twoHopNeighbours.removeAll(uniDirectionalNeighbours.get(mpr));
            biDirectionalNeighbourTwoHopCount.remove(biDirectionalNeighbourTwoHopCount.firstKey());
        }

    }

    /**
     * This method processes the TC messages and uses them to create routing tables
     *
     * @param processedLine The string array will contain the TC message split on "SPACE" to be processed easily.
     */
    private void processTopologyMessage(String[] processedLine) {
        int fromNode = Integer.parseInt(processedLine[1]);
        int sourceNode = Integer.parseInt(processedLine[3]);
        int sequenceNumber = Integer.parseInt(processedLine[4]);

        if (fromNode != nodeId && !topologyTable.containsKey(sourceNode) || (topologyTable.containsKey(sourceNode) && topologyTable.get(sourceNode).get(0) < sequenceNumber)) {
            ArrayList<Integer> topologyInfo = new ArrayList<>();
            topologyInfo.add(sequenceNumber); //sequence number
            topologyInfo.add(fromNode); //from node
            Stream.of(processedLine[7].split(",")).mapToInt(Integer::parseInt).forEach(topologyInfo::add);
            topologyTable.put(sourceNode, topologyInfo);

            tcTimer.put(sourceNode, 0); //update tc timer

            /* FORWARD TC MESSAGE */
            processedLine[1] = String.valueOf(nodeId);
            Stream.of(processedLine)
                    .reduce((partOne, partTwo) -> partOne + SPACE + partTwo)
                    .ifPresent(messagesToSend::add);
        }
    }

    /**
     * This method transmits the TC messages of current node which contain it's MS set for other nodes.
     */
    private void sendTCMessage() {

        //ONLY SEND TC MESSAGES IF MS SET NOT EMPTY
        if (msSet.isEmpty()) return;

        StringBuilder tcMessage = new StringBuilder();
        tcMessage.append("*").append(SPACE).append(nodeId).append(SPACE).append(MESSAGE_TYPE_TC).append(SPACE)
                .append(nodeId).append(SPACE).append(sequenceNumber++).append(SPACE).append("MS").append(SPACE)
                .append("<msnode>").append(SPACE);

        msSet.forEach(meAsMprFor -> tcMessage.append(meAsMprFor).append(","));

        //remove extra ","
        if (tcMessage.lastIndexOf(",") != -1)
            tcMessage.delete(tcMessage.lastIndexOf(","), tcMessage.length());

        tcMessage.append(SPACE).append("<msnode>");

        LOGGER.log(Level.FINEST, "TC MESSAGE : " + tcMessage);

        //ADDED TO QUEUE TO WRITE ALL MESSAGES AT ONCE
        messagesToSend.add(tcMessage.toString());
    }

    /**
     * This method will process the received HELLO messages which tells about neighbours.
     *
     * @param processedLine This string array will contain the received HELLO message split on "SPACE" to process easily.
     */
    private void processHelloMessage(String[] processedLine) {
        int fromNode = Integer.parseInt(processedLine[1]);
        helloTimer.put(fromNode, 0); //reset timer to 0

        Set<Integer> uniDirectionalNeighboursList = Stream.of(processedLine[5].split(","))
                .filter(neighbourId -> !neighbourId.isEmpty())
                .mapToInt(Integer::parseInt)
                .boxed()
                .peek(twoHopNeighbours::add)
                .collect(Collectors.toSet());

        Set<Integer> bidirectionalNeighboursList = Stream.of(processedLine[9].split(","))
                .filter(neighbourId -> !neighbourId.isEmpty())
                .mapToInt(Integer::parseInt)
                .boxed()
                .peek(twoHopNeighbours::add)
                .collect(Collectors.toSet());

        Set<Integer> mprList = Stream.of(processedLine[13].split(","))
                .filter(neighbourId -> !neighbourId.isEmpty())
                .mapToInt(Integer::parseInt)
                .boxed()
                .collect(Collectors.toSet());

        /*If current node is already in bidirectional neighbours list just add the node to current
         * node bi-directional nodes list
         */
        if (bidirectionalNeighboursList.contains(nodeId)) {
            bidirectionalNeighboursList.remove(nodeId);
            biDirectionalNeighbours.put(fromNode, bidirectionalNeighboursList);
        }

        /*If current node is in uni-directional neighbour list add it to bi-directional list
         */
        if (uniDirectionalNeighboursList.contains(nodeId)) {
            uniDirectionalNeighboursList.remove(nodeId);
            uniDirectionalNeighbours.put(fromNode, uniDirectionalNeighboursList);
            biDirectionalNeighbours.put(fromNode, bidirectionalNeighboursList);
        } else {
            uniDirectionalNeighbours.put(fromNode, uniDirectionalNeighboursList);
        }

        /* Add node to MS Set if we are in its MPR set*/
        if (mprList.contains(nodeId)) msSet.add(fromNode);

        /*Remove self as two hop neighbours
         * this is raw info should be sanitized in selecting MPR process
         * */
        twoHopNeighbours.remove(nodeId);
    }

    /**
     * This method sends HELLO messages of current node with its Uni-Directional and Bi-Directional neighbours
     */
    private void sendHelloMessage() {
        StringBuilder helloMessage = new StringBuilder();
        helloMessage.append("*").append(SPACE).append(nodeId).append(SPACE).append(MESSAGE_TYPE_HELLO).append(SPACE)
                .append("UNIDIR").append(SPACE).append(NEIGHBOR).append(SPACE);

        //add unidirectional neighbors
        uniDirectionalNeighbours.keySet().stream()
                .filter(neighbor -> !biDirectionalNeighbours.containsKey(neighbor))
                .forEach(neighbor -> helloMessage.append(neighbor).append(","));

        //remove extra ","
        if (helloMessage.lastIndexOf(",") != -1)
            helloMessage.delete(helloMessage.lastIndexOf(","), helloMessage.length());


        helloMessage.append(SPACE).append(NEIGHBOR).append(SPACE).append("BIDIR").append(SPACE).append(NEIGHBOR).append(SPACE);
        biDirectionalNeighbours.keySet().forEach(neighbor -> helloMessage.append(neighbor).append(","));

        //remove extra ","
        if (helloMessage.lastIndexOf(",") > helloMessage.lastIndexOf("BIDIR"))
            helloMessage.delete(helloMessage.lastIndexOf(","), helloMessage.length());

        helloMessage.append(SPACE).append(NEIGHBOR).append(SPACE).append("MPR").append(SPACE).append(NEIGHBOR).append(SPACE);
        mprSet.forEach(mprNode -> helloMessage.append(mprNode).append(","));

        //remove extra ","
        if (helloMessage.lastIndexOf(",") > helloMessage.lastIndexOf("MPR"))
            helloMessage.delete(helloMessage.lastIndexOf(","), helloMessage.length());

        helloMessage.append(SPACE).append(NEIGHBOR);

        //added to write out queue
        messagesToSend.add(helloMessage.toString());

        LOGGER.log(Level.FINEST, "HELLO MESSAGE : " + helloMessage);
    }

    /**
     * This method will update the nexthop for messages from routig table and forwards them, if no entry is available
     * it is put in waiting queue and retry sending after 30 seconds.
     */
    private void sendDataMessage() {
        if (destinationNodeId == nodeId) return;
        StringBuilder dataMessage = new StringBuilder();
        dataMessage.append(routingTable.containsKey(destinationNodeId) ? routingTable.get(destinationNodeId) : "-1")
                .append(SPACE)
                .append(nodeId).append(SPACE).append(MESSAGE_TYPE_DATA).append(SPACE)
                .append(nodeId).append(SPACE).append(destinationNodeId)
                .append(SPACE).append(data);

        //send message if route data available else wait 30 seconds
        if (routingTable.containsKey(destinationNodeId)) {
            messagesToSend.add(dataMessage.toString());
        } else if (queuedMessages.containsKey(destinationNodeId)) {
            queuedMessages.get(destinationNodeId).add(dataMessage.toString());
        } else {
            ArrayList<String> message = new ArrayList<>();
            message.add(dataMessage.toString());
            queuedMessages.put(destinationNodeId, message);
        }

        LOGGER.log(Level.FINEST, "Message : " + dataMessage);
    }

    /**
     * This method will process the messages in waiting queue and checks if routing entry is available and forwards if available.
     * Else will remain in queue to be retried after 30 seconds.
     */
    private void checkQueuedMessages() {
        if (queuedMessages.isEmpty()) return;

        queuedMessages.forEach((destinationNodeId, messagesList) -> {
            if (routingTable.containsKey(destinationNodeId)) {
                messagesList.forEach((message) -> processDataMessages(message.split(SPACE)));
                queuedMessages.remove(destinationNodeId);
            }
        });

        LOGGER.info("Checked for queued messages");
    }

    /**
     * This method will create mainly 3 files for inter node communication.The 'X' value is taken from program argument <br>
     * fromX.txt <br>
     * toX.txt <br>
     * Xreceived.txt <br>
     */
    private void createFiles() {
        try {
            File resultsDirectory = new File(OUTPUT_DIRECTORY);
            if (!resultsDirectory.exists()) {
                resultsDirectory.mkdir();
            }

            //FILES CREATED BY NODE
            File fromMyNodeFile = new File(OUTPUT_DIRECTORY + "from" + nodeId + DEFAULT_FILE_EXTENSION);
            File toMyNodeFile = new File(OUTPUT_DIRECTORY + "to" + nodeId + DEFAULT_FILE_EXTENSION);
            File myNodeReceivedFile = new File(OUTPUT_DIRECTORY + nodeId + "received" + DEFAULT_FILE_EXTENSION);

            fromMyNodeFile.createNewFile();
            toMyNodeFile.createNewFile();
            myNodeReceivedFile.createNewFile();

            toMyNodeFileReader = new BufferedReader(new FileReader(toMyNodeFile));
            fromMyNodeFileWriter = new BufferedWriter(new FileWriter(fromMyNodeFile, true));
            myNodeReceivedFileWriter = new BufferedWriter(new FileWriter(myNodeReceivedFile));

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e, () -> "Exception in creating files");
        }
    }

    /**
     * This method will write all the messages like TC,HELLO and DATA messages to be forwarded into fromX.txt file
     * this is done once per cycle to avoid I/O performance issues.
     */
    private void transmitMessages() {
        try {
            while (messagesToSend.size() > 0) {
                fromMyNodeFileWriter.write(messagesToSend.poll());
                fromMyNodeFileWriter.newLine();
            }
            fromMyNodeFileWriter.flush();
            myNodeReceivedFileWriter.flush(); //flush from buffer all the messages intended for current node
            messagesToSend.clear();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Failed wrting messages to file in node : " + nodeId, e);
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Node{");
        sb.append("nodeId=").append(nodeId);
        sb.append(", destinationNodeId=").append(destinationNodeId);
        sb.append(", timeToSend=").append(timeToSend);
        sb.append(", data='").append(data).append('\'');
        sb.append(", sequenceNumber=").append(sequenceNumber);
        sb.append(", toMyNodeFileReader=").append(toMyNodeFileReader);
        sb.append(", fromMyNodeFileWriter=").append(fromMyNodeFileWriter);
        sb.append(", myNodeReceivedFileWriter=").append(myNodeReceivedFileWriter);
        sb.append(", uniDirectionalNeighbours=").append(uniDirectionalNeighbours);
        sb.append(", biDirectionalNeighbours=").append(biDirectionalNeighbours);
        sb.append(", topologyTable=").append(topologyTable);
        sb.append(", biDirectionalNeighbourTwoHopCount=").append(biDirectionalNeighbourTwoHopCount);
        sb.append(", twoHopNeighbours=").append(twoHopNeighbours);
        sb.append(", msSet=").append(msSet);
        sb.append(", mprSet=").append(mprSet);
        sb.append(", messagesToSend=").append(messagesToSend);
        sb.append(", routingTable=").append(routingTable);
        sb.append(", helloTimer=").append(helloTimer);
        sb.append(", tcTimer=").append(tcTimer);
        sb.append(", queuedMessages=").append(queuedMessages);
        sb.append('}');
        return sb.toString();
    }
}

package edu.chetan;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class acts as a medium between nodes to transfer messages between them.
 *
 * @author Chetan Garikapati
 */
public class Controller {

    //CONSTANT VARIABLES
    public static final String SPACE = " ";
    public static final String NEIGHBOR = "<neighbor>";
    public static final String MESSAGE_TYPE_DATA = "DATA";
    public static final String MESSAGE_TYPE_TC = "TC";
    public static final String MESSAGE_TYPE_HELLO = "HELLO";
    public static final String OUTPUT_DIRECTORY = System.getProperty("user.home") + "/results/";
    public static final String DEFAULT_FILE_EXTENSION = ".txt";

    private static final String UP = "UP";

    //COntroller class specific variables
    private int timer = 5;
    private final ConcurrentHashMap<Integer, Set<Integer>> linksList = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Integer, HashMap<Integer, Set<Integer>>> futureUpLinks = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Integer, HashMap<Integer, Set<Integer>>> futureDownLinks = new ConcurrentHashMap<>();

    private final HashMap<Integer, BufferedReader> fromNodeReadersList = new HashMap<>();
    private final HashMap<Integer, BufferedWriter> toNodeWritersList = new HashMap<>();
    private final ConcurrentSkipListSet<Integer> recoverStreams = new ConcurrentSkipListSet<>();

    private static final Logger LOGGER = Logger.getLogger("Controller");

    /**
     * Starts the execution of controller for routing messages between nodes based on topology.
     *
     * @param args Requires 1 argument of file path with name for locating topology information.
     * @throws InterruptedException Will be thrown if controller is interrupted when sleeping for 1 second in every cycle.
     */
    public static void main(String[] args) throws InterruptedException {
        if (args.length == 0) {
            LOGGER.severe(() -> "Topology File path is missing");
            System.exit(0);
        } else if (!new File(args[0]).exists()) {
            LOGGER.severe(() -> "Topology File path does not exists");
            System.exit(0);
        }

        Controller controller = new Controller();
        controller.readToplogyFile(args[0]);
        Thread.sleep(5000);
        controller.createReaderAndWritersToNodes();

        while (controller.timer < 120) {
            if (!controller.recoverStreams.isEmpty()) controller.retryStreamRecovery();
            controller.updateLinksList();
            controller.timer++;
            controller.routeMessages();
            Thread.sleep(1000);
            System.out.println("Timer: " + controller.timer);
        }

        LOGGER.log(Level.FINEST, "Controller Debug Info : ", controller);

    }

    /**
     * Reads the topology file and creates the routing links, based on when they are UP or DOWN.
     *
     * @param filePath The path for topology file.
     */
    private void readToplogyFile(String filePath) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(filePath));

            for (String line : lines) {
                int time;
                int node;
                int neighbour;

                String[] topologyEntry = line.split(SPACE);

                time = Integer.parseInt(topologyEntry[0]);
                node = Integer.parseInt(topologyEntry[2]);
                neighbour = Integer.parseInt(topologyEntry[3]);

                if (time <= timer) {
                    if (linksList.containsKey(node)) linksList.get(node).add(neighbour);
                    else {
                        Set<Integer> neighbourLinks = new HashSet<>();
                        neighbourLinks.add(neighbour);
                        linksList.put(node, neighbourLinks);
                    }
                } else if (topologyEntry[1].equals(UP)) {
                    //UP LINK AND HAS ENTRY FOR TIME
                    if (futureUpLinks.containsKey(time)) {
                        if (futureUpLinks.get(time).containsKey(node)) futureUpLinks.get(time).get(node).add(neighbour);
                        else {
                            Set<Integer> neighbourLinks = new HashSet<>();
                            neighbourLinks.add(neighbour);
                            futureUpLinks.get(time).put(node, neighbourLinks);
                        }
                    } else {
                        Set<Integer> neighbourLinks = new HashSet<>();
                        neighbourLinks.add(neighbour);
                        HashMap<Integer, Set<Integer>> futureUpLink = new HashMap<>();
                        futureUpLink.put(node, neighbourLinks);
                        futureUpLinks.put(time, futureUpLink);
                    }
                } else {
                    //DOWN LINK AND HAS ENTRY FOR TIME
                    if (futureDownLinks.containsKey(time)) {
                        if (futureDownLinks.get(time).containsKey(node))
                            futureDownLinks.get(time).get(node).add(neighbour);
                        else {
                            Set<Integer> neighbourLinks = new HashSet<>();
                            neighbourLinks.add(neighbour);
                            futureDownLinks.get(time).put(node, neighbourLinks);
                        }
                    } else {
                        Set<Integer> neighbourLinks = new HashSet<>();
                        neighbourLinks.add(neighbour);
                        HashMap<Integer, Set<Integer>> futureDownLink = new HashMap<>();
                        futureDownLink.put(node, neighbourLinks);
                        futureDownLinks.put(time, futureDownLink);
                    }
                }
            }


        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Toplogy file cannot be opened", e);
        }

    }

    /**
     * Obtains streams between all nodes in the topology to forward messages.
     */
    private void createReaderAndWritersToNodes() {

        for (int nodeId = 0; nodeId < 10; nodeId++) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(OUTPUT_DIRECTORY + "from" + nodeId + DEFAULT_FILE_EXTENSION));
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(OUTPUT_DIRECTORY + "to" + nodeId + DEFAULT_FILE_EXTENSION, true));

                fromNodeReadersList.put(nodeId, bufferedReader);
                toNodeWritersList.put(nodeId, bufferedWriter);

            } catch (Exception e) {
                if (linksList.containsKey(nodeId)) {
                    LOGGER.log(Level.SEVERE, "Failed getting reader/writer stream for node : " + nodeId, e);
                    recoverStreams.add(nodeId);
                }
            }
        }
    }

    /**
     * This method tries to obtain stream to files of nodes if they could not be obatined at the start of the application
     */
    private void retryStreamRecovery() {
        recoverStreams.forEach(nodeId -> {
            try {
                if (!fromNodeReadersList.containsKey(nodeId)) {
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(OUTPUT_DIRECTORY + "from" + nodeId + DEFAULT_FILE_EXTENSION));
                    fromNodeReadersList.put(nodeId, bufferedReader);
                    recoverStreams.remove(nodeId);
                    LOGGER.info("Recovered stream for node : " + nodeId);
                }
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "Recover also failed will retry again for node: " + nodeId, e);
            }
        });
    }

    /**
     * This method updates the links between the nodes to create routing entries.
     */
    private void updateLinksList() {
        if (futureUpLinks.containsKey(timer)) {
            futureUpLinks.get(timer).forEach((node, neighboursList) -> {
                if (linksList.containsKey(node)) linksList.get(node).addAll(neighboursList);
                else {
                    linksList.put(node, neighboursList);
                }
            });
            futureUpLinks.remove(timer);
        }

        if (futureDownLinks.containsKey(timer)) {
            futureDownLinks.get(timer).forEach((node, neighboursList) -> {
                if (linksList.containsKey(node)) linksList.get(node).removeAll(neighboursList);
            });
            futureDownLinks.remove(timer);
        }
    }

    /**
     * This method will route messages from source node to destination nodes based on links between them.
     */
    private void routeMessages() {
        fromNodeReadersList.forEach((node, fromNodeReader) -> {

            try {
                while (fromNodeReader.ready()) {
                    String message = fromNodeReader.readLine();
                    String[] splitMessage = message.split(SPACE);
                    if (splitMessage[0].equals("*")) {
                        //WRITING BROADCAST MESSAGES TO ALL NEIGHBOURS
                        linksList.get(node).forEach(neighbourNode -> {
                            try {
                                if (toNodeWritersList.containsKey(neighbourNode)) {
                                    toNodeWritersList.get(neighbourNode).write(message);
                                    toNodeWritersList.get(neighbourNode).newLine();
                                    toNodeWritersList.get(neighbourNode).flush();
                                }
                            } catch (IOException e) {
                                LOGGER.log(Level.SEVERE, "Failed writing to node : " + neighbourNode, e);
                            } catch (Exception e) {
                                LOGGER.log(Level.SEVERE, "Failed writing from node : " + node + " " + neighbourNode, e);
                            }
                        });
                    } else {
                        //CHECK IF NEXTHOP NEIGHBOUR VALUE LINK IS ACTIVE AND THEN FORWARD OR IGNORE
                        int nextHopNode = Integer.parseInt(splitMessage[0]);
                        if (linksList.get(node).contains(nextHopNode)) {
                            toNodeWritersList.get(nextHopNode).write(message);
                            toNodeWritersList.get(nextHopNode).newLine();
                            toNodeWritersList.get(nextHopNode).flush();
                        }
                    }
                }
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "Failed reading/writing from node : " + node, e);
            }

        });
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Controller{");
        sb.append("timer=").append(timer);
        sb.append(", linksList=").append(linksList);
        sb.append(", futureUpLinks=").append(futureUpLinks);
        sb.append(", futureDownLinks=").append(futureDownLinks);
        sb.append(", fromNodeReadersList=").append(fromNodeReadersList);
        sb.append(", toNodeWritersList=").append(toNodeWritersList);
        sb.append(", recoverStreams=").append(recoverStreams);
        sb.append('}');
        return sb.toString();
    }
}



